# weather-forecast

Quick and dirty shell script for checking the weather. Despite its simplicity, comes in very handy as a command line tool.

### Notes
- Supports locations specified either as U.S. ZIP codes or simple strings.
- In case of an ambiguous location, you can specify additional information.
    - Case in point: `"Oulu"` defaults to the town in Finland, but Oulu, MI can be specified as `"Oulu, Michigan"`.
- The default unit for temperature is Celsius, but Fahrenheit can be specified by giving `F` as the second command line argument.

### Usage
1. Clone the repository, and `chmod +x weather_forecast.sh`.
2. Move the shell script somewhere in your `$PATH`, e.g. `/usr/local/bin`.
3. Optionally, to ensure functionality with non-Bash shells, add the following to your shell config:

```
weather () {
  bash weather_forecast $1 $2
}
```
